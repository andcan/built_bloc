import 'dart:async';

import 'package:async/async.dart';
import 'package:built_value/built_value.dart';

/// Base class for generated [Actions]
abstract class Action<T> {
  final Completer<T> _completer = Completer<T>();

  /// Returns a [Future] which completes when action is executed.
  Future<T> get done => _completer.future;
}

/// Base class for generated BLoCs.
abstract class Bloc<SV extends Built<SV, SB>, SB extends Builder<SV, SB>> {

  /// Returns a new [Bloc].
  ///
  /// Calls init to allow generated mixin to initialize.
  Bloc() {
    init();
  }

  /// Returns initial state for this [Bloc].
  ///
  /// Must be implemented by user.
  SV get initialState;

  /// Extension point for generated mixin t release resources.
  void dispose();

  /// Extension point for generated mixin initialization.
  void init();
}

/// Helpers for [Action].
///
/// This extension is unexported and should be used only by generated code.
extension $Action<T> on Action<T> {

  /// Returns [Action]'s completer.
  Completer<T> get completer => _completer;
}

/// Utility methods for [Result].
extension $Result<T> on Result<T> {

  /// Executes [computation] returning a [Result].
  Result<T> captureSync(T Function() computation) {
    try {
      return Result<T>.value(computation());
    } on Object catch (e) {
      return Result.error(e);
    }
  }
}
