import 'package:test/test.dart';

import 'plugin_tester.dart';

void main() {
  group('corrects abstract', () {
    test('when class is not abstract', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

class _Foo implements Bloc<FooState, FooStateBuilder> {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc<FooState, FooStateBuilder> {
}''');
    });
  });
}
