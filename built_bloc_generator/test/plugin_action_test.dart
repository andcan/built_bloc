import 'package:test/test.dart';

import 'plugin_tester.dart';

void main() {
  group('corrects actions', () {
    test('when action is public', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';
import 'package:built_bloc_annotations/built_bloc_annotations.dart';

part 'test_library.g.dart';

class _Foo implements Bloc<FooState, FooStateBuilder> {

  @action
  void fooAction() {}
}''', r'''
import 'package:built_bloc/built_bloc.dart';
import 'package:built_bloc_annotations/built_bloc_annotations.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc<FooState, FooStateBuilder> {

  @action
  void _fooAction() {}
}''');
    });
  });
}
