import 'package:build/build.dart';
import 'package:built_bloc_generator/built_bloc_generator.dart';

import 'package:logging/logging.dart';
import 'package:source_gen/source_gen.dart';
import 'package:build_test/build_test.dart';

const String pkgName = 'pkg';

final Builder builder = PartBuilder([BuiltBlocGenerator()], '.g.dart');

Future<String> generate(String source) async {
  final sources = <String, String>{
    '$pkgName|lib/bloc.dart': source,
  };

  // Capture any error from generation; if there is one, return that instead of
  // the generated output.
  String error;
  void captureError(LogRecord logRecord) {
    if (logRecord.error is InvalidGenerationSourceError) {
      if (error != null) {
        throw StateError('Expected at most one error.');
      }
      error = logRecord.error.toString();
    }
  }

  final writer = InMemoryAssetWriter();
  await testBuilder(builder, sources,
      rootPackage: pkgName,
      writer: writer,
      onLog: captureError,
      reader: await PackageAssetReader.currentIsolate());
  return error ??
      String.fromCharCodes(
          writer.assets[AssetId(pkgName, 'lib/bloc.g.dart')] ?? []);
}
