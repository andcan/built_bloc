import 'package:test/test.dart';

import 'plugin_tester.dart';

void main() {
  group('corrects part', () {
    test('when part is not referenced', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

abstract class _Foo implements Bloc<FooState, FooStateBuilder> {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';



abstract class _Foo implements Bloc<FooState, FooStateBuilder> {
}''');
    });
  });
}
