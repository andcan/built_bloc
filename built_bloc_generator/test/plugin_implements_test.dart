import 'package:test/test.dart';

import 'plugin_tester.dart';

void main() {
  group('corrects implements statement', () {
    test('with no generics', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc<FooState, FooStateBuilder> {
}''');
    });

    test('with wrong generics', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc<Bar, Baz> {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo implements Bloc<FooState, FooStateBuilder> {
}''');
    });

    test('with interfaces and extends', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar, Bloc<Bar, Baz>, Bop {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar, Bloc<FooState, FooStateBuilder>, Bop {
}''');
    });

    test('with interfaces and no generics', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar, Bloc, Bop {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar, Bloc<FooState, FooStateBuilder>, Bop {
}''');
    });

    test('with generic class', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo<T> implements Bloc {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo<T> implements Bloc<FooState<T>, FooStateBuilder<T>> {
}''');
    });

    test('with awkward formatting', () async {
      await expectCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar<A,
    B>, Bloc,
    Bop {
}''', r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar<A, B>, Bloc<FooState, FooStateBuilder>, Bop {
}''');
    });
  });

  group('does not touch correct implements statement', () {
    test('with awkward formatting', () async {
      await expectNoCorrection(r'''
import 'package:built_bloc/built_bloc.dart';

part 'test_library.g.dart';

abstract class _Foo extends Bar implements Bar<A,
    B>, Bloc<FooState, FooStateBuilder>,
    Bop {
}''');
    });
  });
}
