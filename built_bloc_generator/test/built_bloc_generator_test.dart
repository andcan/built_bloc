import 'package:test/test.dart';

import 'generator_tester.dart';

void main() {
  test('simple bloc', () async {
    expect(await generate('''
import 'package:built_bloc/built_bloc.dart';
import 'package:built_bloc_annotations/built_bloc_annotations.dart';

part 'bloc.g.dart';
    
abstract class _FooBloc implements Bloc<FooBlocState, FooBlocStateBuilder> {

  @action
  void _fooAction(int foo, bool bar, {String fooBar = 'fooBar'}) {
    _builder.foo = foo;
    _builder.bar = bar;
    _builder.fooBar = fooBar;
  }
  
  @action
  Future<void> _asyncFooAction(int foo, bool bar, {String fooBar = 'fooBar'}) async {
    _fooAction(foo, bar, fooBar);
    await Future.delayed(const Duration(milliseconds: 100));
  }
}'''), r'''
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bloc.dart';

// **************************************************************************
// BuiltBlocGenerator
// **************************************************************************

class FooBlocFooAction
    with Action<void>
    implements Built<FooBlocFooAction, FooBlocFooActionBuilder> {
  FooBlocFooAction._();

  factory FooBlocFooAction.build(
      [void Function(FooBlocFooActionBuilder) updates]) = _$FooBlocFooAction;

  factory FooBlocFooAction(
          {@required int foo = null,
          @required bool bar = null,
          String fooBar = 'fooBar'}) =>
      _$FooBlocFooAction._(foo: foo, bar: bar, fooBar: fooBar);

  int get foo;
  bool get bar;
  String get fooBar;
}

class FooBlocAsyncFooAction
    with Action<Future<void>>
    implements Built<FooBlocAsyncFooAction, FooBlocAsyncFooActionBuilder> {
  FooBlocAsyncFooAction._();

  factory FooBlocAsyncFooAction.build(
          [void Function(FooBlocAsyncFooActionBuilder) updates]) =
      _$FooBlocAsyncFooAction;

  factory FooBlocAsyncFooAction(
          {@required int foo = null,
          @required bool bar = null,
          String fooBar = 'fooBar'}) =>
      _$FooBlocAsyncFooAction._(foo: foo, bar: bar, fooBar: fooBar);

  int get foo;
  bool get bar;
  String get fooBar;
}

mixin _$FooBloc on _FooBloc {
  BehaviorSubject<FooBlocState> _$stateSubject;

  final PublishSubject<Action> _$actionSubject = PublishSubject<Action>();

  final FooBlocStateBuilder _builder;

  Sink<FooBlocFooAction> _fooActionSink;

  Sink<FooBlocAsyncFooAction> _asyncFooActionSink;

  void _rebuild() {
    _$stateSubject.add(_builder.build());
  }

  void dispose() {
    _$actionSubject.close();
    _$stateSubject.close();
  }

  void init() {
    _$stateSubject = BehaviorSubject<FooBlocState>.seeded(initialState);
    Observable.merge([
      _$actionSubject.whereType<FooBlocFooAction>().map((action) {
        final result = Result.captureSync(
            () => _fooAction(action.foo, action.bar, fooBar: action.fooBar));
        result.complete(action.completer);
        return result;
      }),
      _$actionSubject.whereType<FooBlocAsyncFooAction>().asyncMap((action) {
        final result = Result.capture(
            _asyncFooAction(action.foo, action.bar, fooBar: action.fooBar));
        result.complete(action.completer);
        return result.asFuture;
      })
    ]).listen((result) {
      _rebuild();
    });
  }

  Sink<FooBlocFooAction> get fooActionSink => _fooActionSink ??=
      DelegatingSink.typed<FooBlocFooAction>(_$actionSubject);
  Future<void> fooAction(int foo, bool bar, {String fooBar = 'fooBar'}) {
    final action = FooBlocFooAction(foo, bar, fooBar: fooBar);
    _$actionSubject.add(action);
    return action.done;
  }

  Sink<FooBlocAsyncFooAction> get asyncFooActionSink => _asyncFooActionSink ??=
      DelegatingSink.typed<FooBlocAsyncFooAction>(_$actionSubject);
  Future<void> asyncFooAction(int foo, bool bar, {String fooBar = 'fooBar'}) {
    final action = FooBlocAsyncFooAction(foo, bar, fooBar: fooBar);
    _$actionSubject.add(action);
    return action.done;
  }
}
''');
  });
}
