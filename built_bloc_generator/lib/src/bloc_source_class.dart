import 'package:analyzer/dart/analysis/results.dart';
import 'package:analyzer/dart/ast/ast.dart' hide Block;
import 'package:analyzer/dart/element/element.dart';
import 'package:built_bloc_generator/src/error.dart';
import 'package:built_bloc_generator/src/type_reference.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value_generator/src/fixes.dart';
import 'package:code_builder/code_builder.dart';
import 'package:quiver/iterables.dart';
import 'package:built_collection/built_collection.dart';
import 'package:source_gen/source_gen.dart' hide LibraryBuilder;

import 'bloc_source_action.dart';

part 'bloc_source_class.g.dart';

abstract class BlocSourceClass
    implements Built<BlocSourceClass, BlocSourceClassBuilder> {
  factory BlocSourceClass(ClassElement element) =>
      _$BlocSourceClass._(element: element);

  BlocSourceClass._();

  @memoized
  ClassDeclaration get classDeclaration =>
      parsedLibrary.getElementDeclaration(element).node as ClassDeclaration;

  ClassElement get element;

  @memoized
  bool get hasPartStatement => source.contains(partStatement);

  @memoized
  bool get isAbstract => element.isAbstract;

  @memoized
  String get name => element.displayName;

  @memoized
  String get publicName => element.displayName.replaceFirst(RegExp(r'^_+'), '');

  @memoized
  String get mixinName => '_\$$publicName';

  @memoized
  ParsedLibraryResult get parsedLibrary =>
      element.library.session.getParsedLibraryByElement(element.library);

  @memoized
  String get partStatement {
    final name = element.library.source.shortName.replaceAll('.dart', '');
    return "part '$name.g.dart';";
  }

  @memoized
  String get source =>
      element.library.definingCompilationUnit.source.contents.data;

  Iterable<GeneratorError> computeErrors() => concat([
        _checkPart(),
        _checkClass(),
      ]);

  @memoized
  BuiltList<BlocSourceAction> get actions =>
      BlocSourceAction.fromBlocSourceClass(this);

  Iterable<GeneratorError> _checkPart() {
    if (hasPartStatement) {
      return [];
    }

    final directives = (classDeclaration.parent as CompilationUnit).directives;
    if (directives.isEmpty) {
      return [
        GeneratorError((b) => b
          ..message = 'Import generated part: $partStatement'
          ..offset = 0
          ..length = 0
          ..fix = '$partStatement\n\n')
      ];
    } else {
      return [
        GeneratorError((b) => b
          ..message = 'Import generated part: $partStatement'
          ..offset = directives.last.offset + directives.last.length
          ..length = 0
          ..fix = '\n\n$partStatement\n\n')
      ];
    }
  }

  String get _generics =>
      genericParameters.isEmpty ? '' : '<${genericParameters.join(', ')}>';

  @memoized
  BuiltList<String> get genericParameters =>
      BuiltList<String>(element.typeParameters.map((e) => e.name));

  @memoized
  String get stateValueClassName => '${publicName}State';

  @memoized
  String get stateBuilderClassName => '${publicName}StateBuilder';

  Iterable<GeneratorError> _checkClass() {
    final result = <GeneratorError>[];

    if (!isAbstract) {
      result.add(GeneratorError((b) => b
        ..message = 'Make class abstract.'
        ..offset = classDeclaration.offset
        ..length = 0
        ..fix = 'abstract '));
    }

    final implementsClause = classDeclaration.implementsClause;
    final expectedInterface =
        'Bloc<${publicName}State$_generics, ${publicName}StateBuilder$_generics>';

    final implementsClauseIsCorrect = implementsClause != null &&
        implementsClause.interfaces
            .any((type) => type.toSource() == expectedInterface);

    if (!implementsClauseIsCorrect) {
      if (implementsClause == null) {
        result.add(GeneratorError((b) => b
          ..message = 'Make class implement $expectedInterface.'
          ..offset = classDeclaration.leftBracket.offset - 1
          ..length = 0
          ..fix = 'implements $expectedInterface'));
      } else {
        var found = false;
        final interfaces = implementsClause.interfaces.map((type) {
          if (type.name.name == 'Bloc') {
            found = true;
            return expectedInterface;
          } else {
            return type.toSource();
          }
        }).toList();
        if (!found) interfaces.add(expectedInterface);

        result.add(GeneratorError((b) => b
          ..message = 'Make class implement $expectedInterface.'
          ..offset = implementsClause.offset
          ..length = implementsClause.length
          ..fix = 'implements ${interfaces.join(", ")}'));
      }
    }

    for (final action in actions) {
      result.addAll(action.computeErrors());
    }

    return result;
  }

  static bool needsBuiltBloc(ClassElement classElement) {
    // TODO: more exact type check.
    return classElement.displayName.startsWith('_') &&
        (classElement.allSupertypes
            .any((interfaceType) => interfaceType.name == 'Bloc'));
  }

  String get builderName => '_builder';

  @memoized
  Reference get referBuilder => refer(builderName);

//  @memoized
//  BuiltList<BlocSourceActionTransformer> get actionTransformers => ValueSourceField.fromClassElements(
//      settings, parsedLibrary, element, builderElement);

  void generateCode(LibraryBuilder libraryBuilder) {
    final errors = computeErrors();
    if (errors.isNotEmpty) {
      throw wrapErrors(errors);
    }
    final classBuilder = ClassBuilder()
      ..name = mixinName
      ..fields.addAll([
        Field(
          (b) => b
            ..name = stateSubjectName
            ..type = referBehaviorSubject(refer(stateValueClassName)),
        ),
        Field(
          (b) => b
            ..name = actionSubjectName
            ..type = referPublishSubject(referAction())
            ..modifier = FieldModifier.final$
            ..assignment =
                referPublishSubject(referAction()).newInstance([]).code,
        ),
        Field((b) => b
          ..name = builderName
          ..type = refer(stateBuilderClassName)
          ..modifier = FieldModifier.final$),
      ])
      ..methods.addAll([
        Method(
          (b) => b
            ..name = '_rebuild'
            ..returns = referVoid
            ..body = Block.of([
              referStateSubject
                  .property('add')
                  .call([referBuilder.property('build').call([])]).statement,
            ]),
        ),
        Method(
          (b) => b
            ..name = 'dispose'
            ..returns = refer('void')
            ..body = Block.of([
              referActionSubject.property('close').call([]).statement,
              referStateSubject.property('close').call([]).statement,
            ]),
        ),
        Method(
          (b) => b
            ..name = 'init'
            ..returns = refer('void')
            ..body = Block.of([
              referStateSubject
                  .assign(referBehaviorSubject(refer(stateValueClassName))
                      .newInstanceNamed('seeded', [refer('initialState')]))
                  .statement,
              referObservable()
                  .newInstanceNamed('merge', [
                    literalList(actions.map((action) => action.initFragment))
                  ])
                  .property('listen')
                  .call([
                    Method(
                      (b) => b
                        ..requiredParameters
                            .add(Parameter((b) => b.name = 'result'))
                        ..body = Block.of([
                          refer('_rebuild').call([]).statement,
                        ]),
                    ).closure,
                  ])
                  .statement,
            ]),
        ),
      ]);
    for (final action in actions) {
      action.generateCode(libraryBuilder, classBuilder);
    }
    libraryBuilder.body.add(classBuilder.build());
  }

  @memoized
  String get stateSubjectName => r'_$stateSubject';

  @memoized
  String get actionSubjectName => r'_$actionSubject';

  @memoized
  Reference get referStateSubject => refer(stateSubjectName);

  @memoized
  Reference get referActionSubject => refer(actionSubjectName);
}

// ignore_for_file: public_member_api_docs
