// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bloc_source_class.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BlocSourceClass extends BlocSourceClass {
  @override
  final ClassElement element;
  ClassDeclaration __classDeclaration;
  bool __hasPartStatement;
  bool __isAbstract;
  String __name;
  String __publicName;
  String __mixinName;
  ParsedLibraryResult __parsedLibrary;
  String __partStatement;
  String __source;
  BuiltList<BlocSourceAction> __actions;
  BuiltList<String> __genericParameters;
  String __stateValueClassName;
  String __stateBuilderClassName;
  Reference __referBuilder;
  String __stateSubjectName;
  String __actionSubjectName;
  Reference __referStateSubject;
  Reference __referActionSubject;

  factory _$BlocSourceClass([void Function(BlocSourceClassBuilder) updates]) =>
      (new BlocSourceClassBuilder()..update(updates)).build();

  _$BlocSourceClass._({this.element}) : super._() {
    if (element == null) {
      throw new BuiltValueNullFieldError('BlocSourceClass', 'element');
    }
  }

  @override
  ClassDeclaration get classDeclaration =>
      __classDeclaration ??= super.classDeclaration;

  @override
  bool get hasPartStatement => __hasPartStatement ??= super.hasPartStatement;

  @override
  bool get isAbstract => __isAbstract ??= super.isAbstract;

  @override
  String get name => __name ??= super.name;

  @override
  String get publicName => __publicName ??= super.publicName;

  @override
  String get mixinName => __mixinName ??= super.mixinName;

  @override
  ParsedLibraryResult get parsedLibrary =>
      __parsedLibrary ??= super.parsedLibrary;

  @override
  String get partStatement => __partStatement ??= super.partStatement;

  @override
  String get source => __source ??= super.source;

  @override
  BuiltList<BlocSourceAction> get actions => __actions ??= super.actions;

  @override
  BuiltList<String> get genericParameters =>
      __genericParameters ??= super.genericParameters;

  @override
  String get stateValueClassName =>
      __stateValueClassName ??= super.stateValueClassName;

  @override
  String get stateBuilderClassName =>
      __stateBuilderClassName ??= super.stateBuilderClassName;

  @override
  Reference get referBuilder => __referBuilder ??= super.referBuilder;

  @override
  String get stateSubjectName => __stateSubjectName ??= super.stateSubjectName;

  @override
  String get actionSubjectName =>
      __actionSubjectName ??= super.actionSubjectName;

  @override
  Reference get referStateSubject =>
      __referStateSubject ??= super.referStateSubject;

  @override
  Reference get referActionSubject =>
      __referActionSubject ??= super.referActionSubject;

  @override
  BlocSourceClass rebuild(void Function(BlocSourceClassBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BlocSourceClassBuilder toBuilder() =>
      new BlocSourceClassBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BlocSourceClass && element == other.element;
  }

  @override
  int get hashCode {
    return $jf($jc(0, element.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BlocSourceClass')
          ..add('element', element))
        .toString();
  }
}

class BlocSourceClassBuilder
    implements Builder<BlocSourceClass, BlocSourceClassBuilder> {
  _$BlocSourceClass _$v;

  ClassElement _element;
  ClassElement get element => _$this._element;
  set element(ClassElement element) => _$this._element = element;

  BlocSourceClassBuilder();

  BlocSourceClassBuilder get _$this {
    if (_$v != null) {
      _element = _$v.element;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BlocSourceClass other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BlocSourceClass;
  }

  @override
  void update(void Function(BlocSourceClassBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BlocSourceClass build() {
    final _$result = _$v ?? new _$BlocSourceClass._(element: element);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
