import 'package:built_collection/built_collection.dart';
import 'package:code_builder/code_builder.dart';

const String coreAsyncUrl = 'dart:async';
const String pkgAsyncUrl = 'package:async/async.dart';
const String pkgBuiltBlocUrl = 'package:built_bloc/built_bloc.dart';
const String pkgBuiltValueUrl = 'package:built_value/built_value.dart';
const String pkgMetaUrl = 'package:meta/meta.dart';
const String pkgRxdartUrl = 'package:rxdart/rxdart.dart';

final Reference referNullableAnnotation = refer('nullable', pkgBuiltValueUrl);

final Reference referProtectedAnnotation = refer('protected', pkgMetaUrl);

final Reference referRequiredAnnotation = refer('required', pkgMetaUrl);

final Reference referVoid = TypeReference((b) => b.symbol = 'void');

Reference referAction([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'Action'
        ..url = pkgBuiltBlocUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

Reference referFuture([Reference typeArgument]) => TypeReference(
      (b) => b
    ..symbol = 'Future'
    ..url = coreAsyncUrl
    ..types = typeArgument == null
        ? ListBuilder<Reference>()
        : ListBuilder<Reference>([typeArgument]),
);

Reference referBehaviorSubject([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'BehaviorSubject'
        ..url = pkgRxdartUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

Reference referBuilder(Reference value, Reference builder) => TypeReference(
      (b) => b
        ..symbol = 'Builder'
        ..url = pkgBuiltValueUrl
        ..types.addAll([
          value,
          builder,
        ]),
    );

Reference referBuilt(Reference value, Reference builder) => TypeReference(
      (b) => b
        ..symbol = 'Built'
        ..url = pkgBuiltValueUrl
        ..types.addAll([
          value,
          builder,
        ]),
    );

Reference referDelegatingSink([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'DelegatingSink'
        ..url = pkgAsyncUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

Reference referResult([Reference typeArgument]) => TypeReference(
      (b) => b
    ..symbol = 'Result'
    ..url = pkgAsyncUrl
    ..types = typeArgument == null
        ? ListBuilder<Reference>()
        : ListBuilder<Reference>([typeArgument]),
);

Reference referObservable([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'Observable'
        ..url = pkgRxdartUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

Reference referPublishSubject([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'PublishSubject'
        ..url = pkgRxdartUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

Reference referSink([Reference typeArgument]) => TypeReference(
      (b) => b
        ..symbol = 'Sink'
        ..url = coreAsyncUrl
        ..types = typeArgument == null
            ? ListBuilder<Reference>()
            : ListBuilder<Reference>([typeArgument]),
    );

// ignore_for_file: public_member_api_docs
