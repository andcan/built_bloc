
import 'package:source_gen/source_gen.dart';
import 'package:built_value_generator/src/fixes.dart';

InvalidGenerationSourceError wrapErrors(Iterable<GeneratorError> errors) {
  final message =
  StringBuffer('Please make the following changes to use BuiltBloc:\n');
  for (var i = 0; i != errors.length; ++i) {
    message.write('\n${i + 1}. ${errors.elementAt(i).message}');
  }

  return InvalidGenerationSourceError(message.toString());
}
