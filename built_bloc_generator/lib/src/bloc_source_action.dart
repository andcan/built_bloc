import 'package:analyzer/dart/element/element.dart';
import 'package:built_bloc_generator/src/error.dart';
import 'package:built_bloc_generator/src/type_reference.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value_generator/src/fixes.dart';
import 'package:code_builder/code_builder.dart';
import 'package:quiver/iterables.dart';

import '../built_bloc_generator.dart';
import 'bloc_source_class.dart';

part 'bloc_source_action.g.dart';

abstract class BlocSourceAction
    implements Built<BlocSourceAction, BlocSourceActionBuilder> {
  factory BlocSourceAction(
          BlocSourceClass blocSourceClass, MethodElement element) =>
      _$BlocSourceAction._(blocSourceClass: blocSourceClass, element: element);

  BlocSourceAction._();

  BlocSourceClass get blocSourceClass;

  @memoized
  String get builderClassName => '${valueClassName}Builder';

  MethodElement get element;

  @memoized
  Expression get callExpression => refer(name).call(
        element.parameters
            .where((parameter) => parameter.isPositional)
            .map((parameter) => refer('action').property(parameter.name)),
        Map<String, Expression>.fromEntries(
          element.parameters.where((parameter) => parameter.isNamed).map(
                (parameter) => MapEntry(
                  parameter.name,
                  refer('action').property(parameter.name),
                ),
              ),
        ),
      );

  @memoized
  Code get initFragment => blocSourceClass.referActionSubject
      .property('whereType')
      .call(const [], const {}, [referValueClass])
      .property(isAsync ? 'asyncMap' : 'map')
      .call([
        Method(
          (b) => b
            ..requiredParameters.add(
              Parameter((b) => b..name = 'action'),
            )
            ..body = Block.of([
              referResult()
                  .property(isAsync ? 'capture' : 'captureSync')
                  .call([
                    isAsync
                        ? callExpression
                        : Method((b) => b.body = callExpression.code).closure
                  ])
                  .assignFinal('result')
                  .statement,
              refer('result')
                  .property('complete')
                  .call([refer('action').property('completer')]).statement,
              (isAsync ? refer('result').property('asFuture') : refer('result'))
                  .returned
                  .statement,
            ]),
        ).closure
      ])
      .code;

  @memoized
  bool get isAsync => element.returnType.isDartAsyncFuture;

  @memoized
  String get name => element.displayName;

  @memoized
  String get publicName => name.replaceFirst(RegExp(r'^_+'), '');

  @memoized
  Reference get referBuilderClass => refer(builderClassName);

  @memoized
  Reference get referReturnType => refer(element.returnType.displayName);

  @memoized
  Reference get referValueClass => refer(valueClassName);

  @memoized
  Reference get referValueImplClass => refer(valueClassImplName);

  @memoized
  String get valueClassImplName => '_\$$valueClassName';

  @memoized
  String get valueClassName => '${blocSourceClass.publicName}'
      '${publicName.replaceRange(0, 1, publicName[0].toUpperCase())}';

  Iterable<GeneratorError> computeErrors() {
    final errors = <GeneratorError>[];

    if (element.isPublic) {
      errors.add(GeneratorError((b) => b
        ..message = 'Make method $name private; add the underscore.'
        ..offset = element.nameOffset
        ..length = element.nameLength
        ..fix = '_$name'));
    }

    return errors;
  }

  void generateCode(LibraryBuilder libraryBuilder, ClassBuilder classBuilder) {
    final errors = computeErrors();
    if (errors.isNotEmpty) {
      throw wrapErrors(errors);
    }
    libraryBuilder.body.add(
      Class(
        (b) => b
          ..name = valueClassName
          ..implements.add(referBuilt(
            referValueClass,
            referBuilderClass,
          ))
          ..mixins.add(referAction(referReturnType))
          ..constructors.addAll([
            Constructor((b) => b.name = '_'),
            Constructor(
              (b) => b
                ..name = 'build'
                ..factory = true
                ..optionalParameters.add(
                  Parameter(
                    (b) => b
                      ..name = 'updates'
                      ..type = refer('void Function($builderClassName)'),
                  ),
                )
                ..redirect = referValueImplClass,
            ),
            Constructor(
              (b) => b
                ..factory = true
                ..lambda = true
                ..optionalParameters.addAll(
                  element.parameters.map(
                    (parameter) => Parameter(
                      (b) => b
                        ..name = parameter.name
                        ..named = true
                        ..annotations = parameter.isRequiredPositional ||
                                parameter.isRequiredNamed
                            ? ListBuilder<Expression>([referRequiredAnnotation])
                            : ListBuilder<Expression>()
                        ..type = refer(parameter.type.displayName)
                        ..defaultTo = Code(parameter.defaultValueCode),
                    ),
                  ),
                )
                ..body = referValueImplClass
                    .newInstanceNamed(
                      '_',
                      const [],
                      Map<String, Expression>.fromEntries(
                        element.parameters.map(
                          (parameter) =>
                              MapEntry(parameter.name, refer(parameter.name)),
                        ),
                      ),
                    )
                    .code,
            ),
          ])
          ..methods.addAll(
            element.parameters.map(
              (parameter) => Method(
                (b) => b
                  ..name = parameter.name
                  ..annotations = parameter.isOptional &&
                          parameter.computeConstantValue().isNull
                      ? ListBuilder<Expression>([referNullableAnnotation])
                      : ListBuilder<Expression>()
                  ..returns = refer(parameter.type.displayName)
                  ..type = MethodType.getter,
              ),
            ),
          ),
      ),
    );
    classBuilder
      ..fields.addAll([
        Field(
          (b) => b
            ..name = '${name}Sink'
            ..type = referSink(referValueClass),
        ),
      ])
      ..methods.addAll([
        Method(
          (b) => b
            ..name = '${publicName}Sink'
            ..returns = referSink(referValueClass)
            ..type = MethodType.getter
            ..lambda = true
            ..body = refer('${name}Sink')
                .assignNullAware(referDelegatingSink().property('typed').call(
                    [blocSourceClass.referActionSubject],
                    const {},
                    [referValueClass]))
                .code,
        ),
        Method(
          (b) => b
            ..name = publicName
            ..returns = isAsync
                ? refer(element.returnType.displayName)
                : referFuture(refer(element.returnType.displayName))
            ..requiredParameters.addAll(
              element.parameters
                  .where((parameter) => parameter.isRequiredPositional)
                  .map(
                    (parameter) => Parameter(
                      (b) => b
                        ..name = parameter.name
                        ..type = refer(parameter.type.displayName),
                    ),
                  ),
            )
            ..optionalParameters.addAll(
              element.parameters.where((parameter) => parameter.isOptional).map(
                    (parameter) => Parameter(
                      (b) => b
                        ..name = parameter.name
                        ..named = parameter.isNamed
                        ..defaultTo = Code(parameter.defaultValueCode)
                        ..type = refer(parameter.type.displayName),
                    ),
                  ),
            )
            ..body = Block.of([
              referValueClass
                  .newInstance(
                    element.parameters
                        .where((parameter) => parameter.isRequiredPositional)
                        .map(
                          (parameter) => refer(parameter.displayName),
                        ),
                    Map<String, Expression>.fromEntries(
                      element.parameters
                          .where((parameter) => parameter.isOptional)
                          .map(
                            (parameter) => MapEntry<String, Expression>(
                              parameter.displayName,
                              refer(parameter.displayName),
                            ),
                          ),
                    ),
                  )
                  .assignFinal('action')
                  .statement,
              blocSourceClass.referActionSubject
                  .property('add')
                  .call([refer('action')]).statement,
              refer('action').property('done').returned.statement,
            ]),
        ),
      ]);
  }

  static BuiltList<BlocSourceAction> fromBlocSourceClass(
    BlocSourceClass blocSourceClass,
  ) =>
      BuiltList<BlocSourceAction>(blocSourceClass.element.methods
          .where((method) => method.metadata
              .map((annotation) => annotation.computeConstantValue())
              .any((value) => value?.type?.displayName == 'BuiltBlocAction'))
          .map((method) => BlocSourceAction(blocSourceClass, method)));
}
