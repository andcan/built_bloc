import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer_plugin/protocol/protocol_common.dart';
import 'package:analyzer_plugin/protocol/protocol_generated.dart';
import 'package:built_bloc_generator/src/bloc_source_class.dart';

class Checker {
  Map<AnalysisError, PrioritizedSourceChange> check(
      LibraryElement libraryElement) {
    final result = <AnalysisError, PrioritizedSourceChange>{};

    for (var compilationUnit in libraryElement.units) {
      // Don't analyze if there's no source; there's nothing to do.
      if (compilationUnit.source == null) {
        continue;
      }
      // Don't analyze generated source; there's nothing to do.
      if (compilationUnit.source.fullName.endsWith('.g.dart')) {
        continue;
      }

      for (final type in compilationUnit.types) {
        if (!type.interfaces.any((i) => i.displayName.startsWith('Bloc'))) {
          continue;
        }

        final sourceClass = BlocSourceClass(type);
        final errors = sourceClass.computeErrors();

        if (errors.isNotEmpty) {
          final lineInfo = compilationUnit.lineInfo;

          // Report one error on the 'Bloc' interface. Bundle together all the
          // necessary fixes.

          final builtNode = sourceClass
              .classDeclaration.implementsClause.interfaces
              .singleWhere((typeName) => typeName.name.name == 'Bloc');
          final offset = builtNode.offset;
          final length = builtNode.length;
          final offsetLineLocation = lineInfo.getLocation(offset);
          final error = AnalysisError(
              AnalysisErrorSeverity.ERROR,
              AnalysisErrorType.LINT,
              Location(
                  compilationUnit.source.fullName,
                  offset,
                  length,
                  offsetLineLocation.lineNumber,
                  offsetLineLocation.columnNumber),
              'Class needs fixes for built_bloc: ${errors.map((error) => error.message).join(' ')}',
              'BUILT_BLOC_NEEDS_FIXES');

          // Fix consists of all the individual fixes, sorted so they apply
          // from the end of the file backwards, so each fix does not
          // invalidate the line numbers for the following fixes.
          final edits = errors
              .where((error) => error.fix != null)
              .map((error) => SourceEdit(error.offset, error.length, error.fix))
              .toList();
          edits.sort((left, right) => right.offset.compareTo(left.offset));

          final fix = PrioritizedSourceChange(
              1000000,
              SourceChange(
                'Apply fixes for built_bloc.',
                edits: [
                  SourceFileEdit(
                    compilationUnit.source.fullName,
                    compilationUnit.source.modificationStamp,
                    edits: edits,
                  )
                ],
              ));
          result[error] = fix;
        }
      }
    }

    return result;
  }
}
