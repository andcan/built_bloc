// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bloc_source_action.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BlocSourceAction extends BlocSourceAction {
  @override
  final BlocSourceClass blocSourceClass;
  @override
  final MethodElement element;
  String __builderClassName;
  Expression __callExpression;
  Code __initFragment;
  bool __isAsync;
  String __name;
  String __publicName;
  Reference __referBuilderClass;
  Reference __referReturnType;
  Reference __referValueClass;
  Reference __referValueImplClass;
  String __valueClassImplName;
  String __valueClassName;

  factory _$BlocSourceAction(
          [void Function(BlocSourceActionBuilder) updates]) =>
      (new BlocSourceActionBuilder()..update(updates)).build();

  _$BlocSourceAction._({this.blocSourceClass, this.element}) : super._() {
    if (blocSourceClass == null) {
      throw new BuiltValueNullFieldError('BlocSourceAction', 'blocSourceClass');
    }
    if (element == null) {
      throw new BuiltValueNullFieldError('BlocSourceAction', 'element');
    }
  }

  @override
  String get builderClassName => __builderClassName ??= super.builderClassName;

  @override
  Expression get callExpression => __callExpression ??= super.callExpression;

  @override
  Code get initFragment => __initFragment ??= super.initFragment;

  @override
  bool get isAsync => __isAsync ??= super.isAsync;

  @override
  String get name => __name ??= super.name;

  @override
  String get publicName => __publicName ??= super.publicName;

  @override
  Reference get referBuilderClass =>
      __referBuilderClass ??= super.referBuilderClass;

  @override
  Reference get referReturnType => __referReturnType ??= super.referReturnType;

  @override
  Reference get referValueClass => __referValueClass ??= super.referValueClass;

  @override
  Reference get referValueImplClass =>
      __referValueImplClass ??= super.referValueImplClass;

  @override
  String get valueClassImplName =>
      __valueClassImplName ??= super.valueClassImplName;

  @override
  String get valueClassName => __valueClassName ??= super.valueClassName;

  @override
  BlocSourceAction rebuild(void Function(BlocSourceActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BlocSourceActionBuilder toBuilder() =>
      new BlocSourceActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BlocSourceAction &&
        blocSourceClass == other.blocSourceClass &&
        element == other.element;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, blocSourceClass.hashCode), element.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BlocSourceAction')
          ..add('blocSourceClass', blocSourceClass)
          ..add('element', element))
        .toString();
  }
}

class BlocSourceActionBuilder
    implements Builder<BlocSourceAction, BlocSourceActionBuilder> {
  _$BlocSourceAction _$v;

  BlocSourceClassBuilder _blocSourceClass;
  BlocSourceClassBuilder get blocSourceClass =>
      _$this._blocSourceClass ??= new BlocSourceClassBuilder();
  set blocSourceClass(BlocSourceClassBuilder blocSourceClass) =>
      _$this._blocSourceClass = blocSourceClass;

  MethodElement _element;
  MethodElement get element => _$this._element;
  set element(MethodElement element) => _$this._element = element;

  BlocSourceActionBuilder();

  BlocSourceActionBuilder get _$this {
    if (_$v != null) {
      _blocSourceClass = _$v.blocSourceClass?.toBuilder();
      _element = _$v.element;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BlocSourceAction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BlocSourceAction;
  }

  @override
  void update(void Function(BlocSourceActionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BlocSourceAction build() {
    _$BlocSourceAction _$result;
    try {
      _$result = _$v ??
          new _$BlocSourceAction._(
              blocSourceClass: blocSourceClass.build(), element: element);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'blocSourceClass';
        blocSourceClass.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BlocSourceAction', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
