library built_bloc_generator;

import 'dart:async';

import 'package:analyzer/dart/element/type_system.dart';
import 'package:async/async.dart';
import 'package:build/build.dart';
import 'package:built_bloc_generator/src/bloc_source_class.dart';
import 'package:built_value/built_value.dart';
import 'package:code_builder/code_builder.dart' as code_builder;
import 'package:dart_style/dart_style.dart';
import 'package:meta/meta.dart';
import 'package:source_gen/source_gen.dart';

String _error(Object error) {
  var lines = '$error'.split('\n');
  var indented = lines.skip(1).map((l) => '//        $l'.trim()).join('\n');
  return '// Error: ${lines.first}\n$indented';
}

class BuiltBlocGenerator extends Generator {
  const BuiltBlocGenerator();

  @override
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) async {
    final libraryBuilder = code_builder.LibraryBuilder();

    final blocClasses = library.classes
        .where(BlocSourceClass.needsBuiltBloc)
        .map((e) => BlocSourceClass(e))
        .toList(growable: false);
    for (final sourceClass in blocClasses) {
      try {
        sourceClass.generateCode(libraryBuilder);
      } catch (e, st) {
        libraryBuilder.body.add(code_builder.Code(_error(e)));
        log.severe(
            'Error in BuiltBlocGenerator for ${sourceClass.element}.', e, st);
      }
    }
    final emitter =
        code_builder.DartEmitter(code_builder.Allocator.none);
    var source = libraryBuilder.build().accept(emitter).toString();
    for (final sourceClass in blocClasses) {
      source = source.replaceFirst('class ${sourceClass.mixinName}',
          'mixin ${sourceClass.mixinName} on ${sourceClass.name}');
    }
    return DartFormatter(fixes: StyleFix.all).format(source);
  }
}
