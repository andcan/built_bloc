import 'dart:isolate';

import 'package:analyzer/file_system/physical_file_system.dart';
import 'package:analyzer_plugin/starter.dart';
import 'package:built_bloc_generator/src/plugin/plugin.dart';

void start(List<String> args, SendPort sendPort) {
  ServerPluginStarter(
          BuiltBlocAnalyzerPlugin(PhysicalResourceProvider.INSTANCE))
      .start(sendPort);
}
